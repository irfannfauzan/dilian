import 'package:findapps/loginpage.dart';
import 'package:findapps/pages/splash_page.dart';
import 'package:findapps/splashscreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';

class Authenticate extends StatelessWidget {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    if (_auth.currentUser != null) {
      return SplashLogin();
    } else {
      return SplashScreen();
    }
  }
}
